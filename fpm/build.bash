#!/bin/bash

# idempotent function
function setup_fpm {
  if [ ! -d release ] ; then
      mkdir release
      sudo apt-get --yes --force-yes install ruby ruby-dev rubygems build-essential
      sudo gem install --no-ri --no-rdoc fpm
  fi 

}


function build_fpm {
   fpm -n osvr -v $1 -s dir -t deb -p /opt/osvr-ubuntu-build-script/release --after-install=fpm/after-install.bash /opt/build/dist/=/opt/osvr
}

## === Argument Handling === ##
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

VERSION=0.1.0
use_help=false

while getopts "v:h" opt; do
    case "$opt" in
    v)  
        VERSION=$OPTARG
        ;;  
    h)  use_help=true
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

if [ $use_help == true ] ; then
  echo 'Usage: build [-v <version>]'
  echo '-v the version to build'
  echo '-h to view help'
  exit 0;
fi

setup_fpm
build_fpm $VERSION
