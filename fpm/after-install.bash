#!/bin/bash
vrpathreg adddriver /opt/osvr/lib/openvr/osvr

#echo 'set the following in ~/.local/share/Steam/config/steamvr.vrsettings'

# jq will be good to update it but need to install jq
SETTINGS_FILE="~/.local/share/Steam/config/steamvr.vrsettings"

bash -c "cat ${SETTINGS_FILE} | /opt/osvr/bin/jq 'driver_osvr.scanoutOrigin = \"lower-right\"' > /tmp/tmp.json"
bash -c "cat /tmp/tmp.json    | /opt/osvr/bin/jq 'serverTimeout = 60' > /tmp/tmp.json"
bash -c "cat /tmp/tmp.json    | /opt/osvr/bin/jq 'verbose = true' > /tmp/tmp.json"
cp /tmp/tmp.json $SETTINGS_FILE


#{
#   "driver_osvr" : { 
#      "scanoutOrigin" : "lower-right",
#      "serverTimeout" : 60, 
#      "verbose" : true
#   },  
#   "perfcheck" : { 
#      "linuxGPUProfiling" : true,
#      "warnOnlyOnce" : true
#   },  
#   "steamvr" : { 
#      "allowInterleavedReprojection" : false,
#      "directModeEdidPid" : 4121,
#      "directModeEdidVid" : 53838,
#      "enableHomeApp" : false
#   }   
#}

