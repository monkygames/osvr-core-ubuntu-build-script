module MyVars
  OS        = "puppetlabs/ubuntu-16.04-64-puppet"
  OS_URL    = "https://atlas.hashicorp.com/puppetlabs/boxes/ubuntu-16.04-64-puppet"
  BUILD_DIR = "xenial64"
end

