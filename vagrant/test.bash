#!/bin/bash

TESTS=(
"ubuntu16.04"
)
OUT="results.txt"

rm $OUT

function cleanup {
    vagrant destroy -f
}

function test_os {
  echo "require './config/${1}'" > config/testos.rb
  cat config/testos.rb
}

# Provisions the vm and run the provisioner
function run_test {
    scripts/run.bash
    if [ $? -eq 0 ] ; then
        echo "${1} - passed"
        echo "${1} - passed" >> $OUT
    else
        echo "${1} - failed"
        echo "${1} - failed" >> $OUT
    fi
}

# Tests the provision (ie run the provisioner the 2nd time)
function provision_test {
    scripts/provision.bash
    if [ $? -eq 0 ] ; then
        echo "${1} - passed"
        echo "${1} - passed" >> $OUT
    else
        echo "${1} - failed"
        echo "${1} - failed" >> $OUT
    fi
}

for i in "${TESTS[@]}"
do
    test_os $i
    run_test $i
    #provision_test $i
    cleanup
done
