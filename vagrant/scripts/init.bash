#!/bin/bash

if [ ! -e /home/vagrant/.provision.txt ] ; then
    curl --remote-name --location https://apt.puppetlabs.com/DEB-GPG-KEY-puppet
    gpg --keyid-format 0xLONG --with-fingerprint ./DEB-GPG-KEY-puppet
    apt-key add DEB-GPG-KEY-puppet
    apt-get update
    apt-get install --yes lsb-release
    touch /home/vagrant/.provision.txt
fi

