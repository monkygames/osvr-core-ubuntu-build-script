#!/bin/bash
 
vagrant provision 

if [ $? -eq 0 ] ; then
    echo "*** Deployed without errors ***"
    exit 0
else
    echo "*** Deployment has ERRORs. ***" >&2
    exit 1
fi
