#!/bin/bash
# install a build
cp -r build/xenial64/dist/lib /opt/osvr/.
cp -r build/xenial64/dist/include /opt/osvr/.
cp -r build/xenial64/dist/bin /opt/osvr/.

# add steamvr driver
echo 'add path to steamvr'
#vrpathreg adddriver /opt/osvr/lib/openvr/osvr

echo 'need to set the following'
echo 'set the following in ~/.local/share/Steam/config/steamvr.vrsettings'
echo 'see comments'
#"driver_osvr": {
#    "verbose": true,
#    "scanoutOrigin": "lower-right"
#}
