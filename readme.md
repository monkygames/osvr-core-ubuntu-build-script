# OSVR: Ubuntu Build Script

## About
This project contains a build script for installing OSVR for the Ubuntu Series of Operating Systems.  This script is based on the documentation for installing OSVR on Ubuntu 14.04 [here](https://github.com/OSVR/OSVR-Core/wiki/Linux-Build-Instructions#known-issues-temporary).  The general overview for installing OSVR on linux can be found [here](http://osvr.github.io/doc/installing/linux/).


## Instructions
Run the following command in a terminal.

```
./build.bash
```

Build options

```
Usage: build [-d <path to root directory>] [-s]
-d the root directory to install (optional)
-s to build SteamVR-OSVR Plugin
-n to build NolorVR-OSVR Plugin
-f to build OSVR-Fusion Plugin
-r to rebuild not using clean
-l the latest tag (not master branch
-i the directory that contains the bash libs
-h to view help
```

## Usage
To run the OSVR server run the following command from a terminal.

```
./run.bash
```

## Test Suite
The test suite leverages vagrant and bash scripts to compile OSVR-Core for multiple versions of operating systems.  To kick off the test suite, do the following.

### Setup
The following software is required before running the test

* [Vagrant](https://www.vagrantup.com/downloads.html)
* [Virtual box](https://www.virtualbox.org/wiki/Downloads)

### Supported Operating Systems
The testing scripts are written in Bash. So the following OS's should work out of the box.
* GNU/Linux
* OSX

I suspect that once the Ubuntu on Windows 10 stuff is worked out, that eventually this will work on Windows 10 (who really will do this though)?

### Test Instructions

* cd to the vagrant directory
* run the test script 
```
./test.bash
```
* after test complete, view results
```
cat results.txt
```

The directory that contains the builds for all OS versions is located in the vagrant/build directory.
The sub dirs in the build represents the operating system that was built.  For instance:
```
vagrant/build/precise64
```
Is the build for Ubuntu 12.04.

## Supported Operating Systems
* Ubuntu 12.04
* Ubuntu 14.04
* Ubuntu 16.04

## Distribution Packaging
See fpm directory -- TODO
