#!/bin/bash

# prep
if [ -d files ] ; then
    rm -rf files
fi
mkdir files
mkdir files/conf
cp conf/* files/conf/.
cp files/conf/NOFCv0.2_hdk2.all_in_one.json files/osvr_server_config.json

# delete old server config
if [ -e '../vagrant/build/xenial64/dist/osvr_server_config.json' ] ; then
    rm '../vagrant/build/xenial64/dist/osvr_server_config.json'
fi


# build
snapcraft clean
pushd .
cd snap
snapcraft clean
snapcraft
#sudo snap install --devmode osvr-nofc_0.2_amd64.snap
popd
