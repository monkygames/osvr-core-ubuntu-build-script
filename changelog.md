# Release 0.2.1 - 2017/03/10
## Summary
  Fixed SteamVR-OSVR compilation issues

### Changed
  - git submodule command was moved after the git branch command
  - created a script for registering with steamvr

# Release 0.2.0 - 2017/03/10
## Summary
  Reformatted code base into separate files.

### Changed
  - Created a new directory bash_lib
  - Created files in bash_lib based on the code that is compiled
  - Updated vagrant file to handle importing the bash_lib files
  - Commented out building openvr as SteamVR linux version is 'working' with steam beta client
  - Added PR for osvr core's tracking code
  - Added libuvc for new tracking code
  - Added PR for SteamVR-OSVR to work with SteamVR Linux Beta
