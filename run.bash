#!/bin/bash

INSTALL_DIR=$(pwd)/dist
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$INSTALL_DIR/lib

pushd .
cd $INSTALL_DIR/bin
./osvr_server ../osvr_server_config.json
#./osvr_server ../osvr_server_config.HDK20ExtendedLandscape.sample.json
#./osvr_server ../osvr_server_config.HDK20ExtendedPortrait.sample.json
#./osvr_server ../osvr_server_config.HDK20DirectMode.sample.json

popd
