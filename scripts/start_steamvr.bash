#!/bin/bash

PWD=$PWD
export openvr=$PWD/openvr
export steam=~/.local/share/Steam
export steamvr=$steam/SteamApps/common/SteamVR
export osvr=$PWD/dist

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:
LD_LIBRARY_PATH+=/usr/lib/:
LD_LIBRARY_PATH+=/usr/lib32/:
LD_LIBRARY_PATH+=$openvr/lib/linux32/:
LD_LIBRARY_PATH+=$openvr/lib/linux64/:
LD_LIBRARY_PATH+=$steam/ubuntu12_32/:
LD_LIBRARY_PATH+=$steamvr/bin/linux32/:
LD_LIBRARY_PATH+=$steamvr/bin/linux64/:
LD_LIBRARY_PATH+=$osvr/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH

#strace $steamvr/bin/linux64/vrcmd 2>&1 | grep -i osvr
#$steamvr/bin/linux64/vrserver --keepalive
$steamvr/bin/linux64/vrserver
