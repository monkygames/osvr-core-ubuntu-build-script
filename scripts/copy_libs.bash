#!/bin/bash

BASE=/home/spethm/Work/MonkyGames/projects/osvr/osvr-ubuntu-build-script/dist/lib

function symlink_lib {
  cp $BASE/$1 $DEST/.
}

DEST=$1

symlink_lib libosvrClient.so
symlink_lib libosvrCommon.so
symlink_lib libosvrJointClientKit.so
symlink_lib libosvrPluginHost.so
symlink_lib libosvrPluginKit.so
symlink_lib libosvrServer.so
symlink_lib libosvrUSBSerial.so
symlink_lib libosvrUtil.so
symlink_lib libosvrVRPNServer.so
symlink_lib libosvrRenderManager.so
