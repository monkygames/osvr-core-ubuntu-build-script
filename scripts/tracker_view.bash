#!/bin/bash

INSTALL_DIR=$(pwd)/dist
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$INSTALL_DIR/lib

pushd .
cd $INSTALL_DIR
#./OSVRTrackerView
#./OSVRTrackerView --pose /me/head
./OSVRTrackerView --orientation /me/head

popd
