#!/bin/bash

INSTALL_DIR=/opt/osvr
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$INSTALL_DIR/lib

pushd .
cd $INSTALL_DIR/bin
./osvr_server ../osvr_server_config.json
popd
