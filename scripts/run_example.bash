#!/bin/bash

DISPLAY_PANEL=':0.0'
#DISPLAY_PANEL=':0.1'

INSTALL_DIR=/opt/osvr
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$INSTALL_DIR/lib

export osvr=$INSTALL_DIR
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:
LD_LIBRARY_PATH+=/usr/lib/:
LD_LIBRARY_PATH+=/usr/lib32/:
LD_LIBRARY_PATH+=$osvr/lib/:
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH

pushd .
cd $INSTALL_DIR/bin
#./RenderManagerOpenGLCoreExample
#./RenderManagerOpenGLPresentExample
#./osvr_reset_yaw

DISPLAY=$DISPLAY_PANEL ./RenderManagerOpenGLSharedContextPresentExample

popd
