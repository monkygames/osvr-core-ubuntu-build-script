#!/bin/bash

INSTALL_DIR=$(pwd)/dist
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$INSTALL_DIR/lib

pushd .
cd $INSTALL_DIR
./bin/vrpn_print_devices org_osvr_filter_videoimufusion/HeadFusion@localhost:3883

popd
