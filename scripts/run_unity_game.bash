#!/bin/bash

#DISPLAY_PANEL=':0.1'
DISPLAY_PANEL=':0.0'

function run_naufrage {
  cd /home/spethm/Downloads/VR/games/naufrage
  ./naufrage.x86_64
}

function run_gunspinner {
  cd /home/spethm/Downloads/VR/games/GunSpinningVR_0.5.1-linux
  ./GunSpinningVR.x86_64 -vrmode None
}

function run_hell {
  cd /home/spethm/Downloads/VR/games/HellicottCity_Linux
  ./HellicottCity_Linux.x86
}

function run_myth {
  cd /home/spethm/Downloads/VR/games/mythgreenvr_0.9.9-linux
  ./MythgreenVR.x86_64
}

function run_mars {
  cd /home/spethm/Downloads/VR/games/marsextraction_1.8.9.2-linux-SHAREWARE
  ./M.A.R.S.\ Extraction.x86_64 
}

function run_palace {
  cd /home/spethm/Downloads/VR/games/OSVR-Unity-Palace-Demo-Build
  #./OSVR-Unity-Palace-Demo.x86_64
  DISPLAY=$DISPLAY_PANEL ./OSVR-Unity-Palace-Demo.x86_64
}

PWD=$PWD
export openvr=$PWD/openvr
export osvr=$PWD/dist

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:
LD_LIBRARY_PATH+=/usr/lib/:
LD_LIBRARY_PATH+=/usr/lib32/:
LD_LIBRARY_PATH+=$openvr/lib/linux32/:
LD_LIBRARY_PATH+=$openvr/lib/linux64/:
LD_LIBRARY_PATH+=$osvr/lib/:
LD_LIBRARY_PATH+=~/.steam/bin32
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH

pushd .
  #run_naufrage
  #run_gunspinner
  #run_hell
  #run_myth
  #run_mars
  run_palace
popd
