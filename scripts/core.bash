#!/bin/bash

# A library for compiling the OSVR core

## ==== Functions ==== ##
function setup {
  if [ ! -e $INSTALL_DIR ] ; then
    mkdir $INSTALL_DIR
  fi

  # Adding cmake ppa because requires cmake 3.x 
  # Ubuntu 14.04 only has cmake 2.x
  sudo add-apt-repository ppa:george-edison55/cmake-3.x
  sudo add-apt-repository ppa:ubuntu-toolchain-r/test
  sudo apt-get update

  ## boost dependancies
  BOOST="libboost-dev libboost-thread-dev libboost-program-options-dev libboost-filesystem-dev"

  # The rest of the depndancies
  DEPS="cmake ${BOOST} libopencv-dev python2.7 libusb-1.0-0-dev gcc-5 g++-5 git-core"
  sudo apt-get --yes --force-yes install $DEPS


  # libfunctionality
  git clone --recursive https://github.com/OSVR/libfunctionality.git
  if [ ! -e libfunctionality/build ] ; then
    mkdir libfunctionality/build
  fi

  # jsoncpp
  git clone --recursive https://github.com/VRPN/jsoncpp
  if [ ! -e jsoncpp/build ] ; then
    mkdir jsoncpp/build
  fi

  # building osvr-core
  git clone --recursive https://github.com/OSVR/OSVR-Core.git
  if [ ! -e OSVR-Core/build ] ; then
    mkdir OSVR-Core/build
  fi
  echo > setup.done
}

function build_libfunctionality {
  pushd .
  cd libfunctionality/build
  cmake -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" ..
  make all install
  popd
}

function build_jsoncpp {
  pushd .
  cd jsoncpp/build
  cmake .. -DJSONCPP_WITH_CMAKE_PACKAGE=ON -DJSONCPP_LIB_BUILD_SHARED=OFF -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}"
  make install
  popd
}

function build_osvr {
  pushd .
  cd OSVR-Core/build
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" ..
  make install
  popd
}

function clean {
  pushd .
  cd $1/build
  make clean
  popd
}
