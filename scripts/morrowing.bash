#!/bin/bash

PWD=$PWD
export openvr=$PWD/openvr
export osvr=$PWD/dist

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:
LD_LIBRARY_PATH+=/usr/lib/:
LD_LIBRARY_PATH+=/usr/lib32/:
LD_LIBRARY_PATH+=$openvr/lib/linux32/:
LD_LIBRARY_PATH+=$openvr/lib/linux64/:
LD_LIBRARY_PATH+=$osvr/lib/:
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH

pushd .
cd ~/Downloads/VR/games/TESUnity-0.5-linux64
#./TESUnity.x86_64
./TESUnity.x86_64 --vrmode None
popd
