#!/bin/bash
~/.local/share/Steam/SteamApps/common/SteamVR/bin/linux64/vrpathreg adddriver /opt/osvr/lib/openvr/osvr

# Add library to the path so that steam can find it
echo '/opt/osvr/lib' | sudo tee -a /etc/ld.so.conf.d/osvr.conf
ldconfig