#!/bin/bash

# idempotent function
function setup_open_scenegraph {
  if [ ! -d OpenSceneGraph ] ; then
    git clone --recursive https://github.com/openscenegraph/OpenSceneGraph
  fi
  if [ ! -e OpenSceneGraph/build ] ; then
      mkdir OpenSceneGraph/build
  fi
}

function build_open_scenegraph {
  pushd .
  cd OpenSceneGraph
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  cmake -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_INSTALL_LIBDIR=lib ..
  make install
  popd
  # copy all lib64 into lib
  cp -r dist/lib64/* dist/lib/.
}
