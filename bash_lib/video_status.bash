#!/bin/bash

function setup_video_status {
  if [ ! -d OSVR-HDK-Video-Status ] ; then 
    git clone https://github.com/sensics/OSVR-HDK-Video-Status
  fi

  if [ ! -e OSVR-HDK-Video-Status/build ] ; then
    mkdir OSVR-HDK-Video-Status/build
  fi
}

function build_video_status {
  pushd .
  cd OSVR-HDK-Video-Status
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" ..
  make
  #make install
  popd
}
