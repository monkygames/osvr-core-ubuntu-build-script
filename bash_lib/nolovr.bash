#!/bin/bash

# idempotent function
function setup_nolovr {
  if [ ! -d nolo-osvr ] ; then
    #git clone --recursive https://github.com/lonetech/nolo-osvr
    #git clone --recursive https://github.com/nanospork/nolo-osvr
    git clone --recursive https://github.com/conzar/nolo-osvr
  fi 

  if [ ! -d nolo-osvr/build ] ; then
    mkdir nolo-osvr/build
  fi
}


function build_nolovr {
  pushd .
  cd nolo-osvr
  get_head
  git pull
  cd build
  cmake .. -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}"
  make install
  popd
}
