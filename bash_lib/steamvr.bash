#!/bin/bash

function setup_steamvr {
  # SteamVR-OSVR
  if [ ! -d SteamVR-OSVR ] ; then
    #git clone --recursive https://github.com/OSVR/SteamVR-OSVR
    git clone -b controller-integration --recursive https://github.com/Conzar/SteamVR-OSVR.git
    pushd .
    cd SteamVR-OSVR
    ### need to update cmake
    #echo 'set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fpic")' >> CMakeLists.txt
    popd

  fi

  if [ ! -e SteamVR-OSVR/build ] ; then
    mkdir SteamVR-OSVR/build
  fi


}

function build_steamvr {
  pushd .
  cd SteamVR-OSVR

  git pull
#  if [ $LATEST_TAG == true ] ; then
#      check_latest_tag
#  else
#      get_head
#  fi

  # checkout the an experimental branch b/c master branch doesn't compile
  #git checkout update-v1.0.6

  git submodule update --init --recursive

 
  # pull a fix
  #git pull origin pull/108/head --no-edit
  
  cd build
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" ..
  make install
  popd
}
