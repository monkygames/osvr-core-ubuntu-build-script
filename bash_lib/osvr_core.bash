#!/bin/bash
# Managing setting up and building osvr-core

# idempotent function
function setup_osvr_core {
  if [ ! -d OSVR-Core ] ; then

    git clone --recursive https://github.com/OSVR/OSVR-Core.git
    #git clone --recursive https://github.com/ChristophHaag/OSVR-Core.git
    #git clone --depth=50 --branch=appveyor https://github.com/OSVR/OSVR-Core.git OSVR/OSVR-Core
    #git submodule update --init --recursive
  fi

  if [ ! -e OSVR-Core/build ] ; then
    mkdir OSVR-Core/build
  fi
}

function build_osvr {
  pushd .
  cd OSVR-Core
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi

  if [ $TRACKER_FIX == true ] ; then
    # positional tracking videobasedtracker
    git pull origin pull/492/head --no-edit

    # positional tracking unifiedvideoinertialtracker
    git pull origin pull/493/head --no-edit
  fi

  cd build
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" ..
  make install
  popd
}