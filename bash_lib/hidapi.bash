#!/bin/bash

# idempotent function
function setup_hidapi {
  if [ ! -d hidapi ] ; then
    git clone --recursive https://github.com/signal11/hidapi
  fi 

  if [ ! -d hidapi/build ] ; then
    mkdir hidapi/build
    if [ $INSTALL_DEPS ] ; then
        apt-get install -y libudev-dev libusb-1.0-0-dev autotools-dev autoconf automake libtool
    fi
  fi
}


function build_hidapi {
  pushd .
  cd hidapi
  get_head
  git pull
  #cd build
  ./bootstrap
  ./configure --prefix="${INSTALL_DIR}"
  make
  make install
  popd
}
