#!/bin/bash
# Contains common functions used throughout codebase.
##

## Checks out the latest tag
function check_latest_tag {
  # Checkout the latest tag
  git fetch --tags

  # get the latest tag
  latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)

  # get the latest tag
  git checkout $latestTag
  git submodule update --init --recursive
}

function check_latest_tag_only {
  # Checkout the latest tag
  git fetch --tags

  # get the latest tag
  latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)

  # get the latest tag
  git checkout $latestTag
}

# gets the latest from git head
function get_head {
  git checkout master
  git submodule update --init --recursive
  git pull
}

function get_head_only {
  git checkout master
  git pull
}

function clean {
  pushd .
  cd $1/build
  make clean
  popd
}
