#!/bin/bash

## Manages the libfunctionality dependency

# idempotent function
function setup_libfunctionality {

  if [ ! -d libfunctionality/build ] ; then
    git clone --recursive https://github.com/OSVR/libfunctionality.git
  fi

  if [ ! -d libfunctionality/build ] ; then
    mkdir libfunctionality/build
  fi
}

function build_libfunctionality {
  pushd .
  cd libfunctionality
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  cmake -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" ..
  make all install
  popd
}