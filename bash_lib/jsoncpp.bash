#!/bin/bash

# idempotent function
function setup_jsoncpp {
  # jsoncpp
  if [ ! -d jsoncpp ] ; then
    git clone --recursive https://github.com/VRPN/jsoncpp
  fi 

  if [ ! -d jsoncpp/build ] ; then
    mkdir jsoncpp/build
  fi
}


function build_jsoncpp {
  pushd .
  cd jsoncpp
  get_head
  git pull
  cd build
  cmake .. -DJSONCPP_WITH_CMAKE_PACKAGE=ON -DJSONCPP_LIB_BUILD_SHARED=OFF -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}"
  make install
  popd
}