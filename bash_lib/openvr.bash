#!/bin/bash

# sets up the open vr fork from haag
function setup_openvr {
  if [ ! -d openvr ] ; then
    git clone https://github.com/ChristophHaag/openvr.git
  fi
  if [ ! -e openvr/build ] ; then
    mkdir openvr/build
  fi
}

function build_openvr {
  pushd .
  cd openvr
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  cmake -DCAME_BUILD_TYPE=Release $ROOT_DIR/openvr
  #cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" ..
  make
  #make install
  popd
}

