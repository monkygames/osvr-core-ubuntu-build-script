#!/bin/bash

# Manages setting up and building the render manager

# idempotent function
function setup_render_manager {
  if [ ! -d OSVR-RenderManager ] ; then
    git clone https://github.com/sensics/OSVR-RenderManager.git
  fi

  if [ ! -e OSVR-RenderManager/build ] ; then
    mkdir OSVR-RenderManager/build
  fi
}


function build_rendermanager {
  pushd .
  cd $RENDER_MANAGER_DIR
  #get_head_only
  #check_latest_tag_only
  git submodule init vendor/vrpn
  git submodule update
  git pull
  cd build
  #cmake -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" ..
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" ..
  make install
  popd
}