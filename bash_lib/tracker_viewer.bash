#!/bin/bash

# idempotent function
function setup_tracker_viewer {
  if [ ! -d OSVR-Tracker-Viewer ] ; then
    git clone --recursive https://github.com/OSVR/OSVR-Tracker-Viewer
  fi

  if [ ! -e OSVR-Tracker-Viewer/build ] ; then
    mkdir OSVR-Tracker-Viewer/build
  fi
}

function build_tracker_viewer {
  pushd .
  cd OSVR-Tracker-Viewer
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" ..
  make install
  popd
}
