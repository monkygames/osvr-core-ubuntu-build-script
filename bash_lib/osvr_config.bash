#!/bin/bash

# .NET so don't use 

function osvr_config_setup {
  # SteamVR-OSVR
  if [ ! -d OSVR-Config ] ; then
    git clone --recursive https://github.com/OSVR/OSVR-Config
  fi

  if [ ! -e OSVR-Config/build ] ; then
    mkdir OSVR-Config/build
  fi

  ## Shoot yourself in the face, installing .net :(

  ### if ubuntu 14.04
  sudo sh -c 'echo "deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/dotnet-release/ trusty main" > /etc/apt/sources.list.d/dotnetdev.list'
  sudo apt-key adv --keyserver apt-mo.trafficmanager.net --recv-keys 417A0893
  sudo apt-get update

  ### if ubuntu 16.04
  #sudo sh -c 'echo "deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/dotnet-release/ xenial main" > /etc/apt/sources.list.d/dotnetdev.list'
  #sudo apt-key adv --keyserver apt-mo.trafficmanager.net --recv-keys 417A0893
  #sudo apt-get update

  sudo apt-get install -y dotnet-dev-1.0.0-preview2-003121
  # update to latest npm
  curl -sL https://deb.nodesource.com/setup | sudo bash -
  sudo apt-get install -y nodejs
  sudo npm update -g npm
  sudo npm install -g bower 
  sudo npm install -g gulp 
}
