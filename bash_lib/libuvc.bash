#!/bin/bash

function setup_libuvc {
  LIB_DIR=libuvc
  if [ ! -d $LIB_DIR ] ; then
    git clone https://github.com/ktossell/libuvc
  fi

  if [ ! -d $LIB_DIR/build ] ; then
    mkdir $LIB_DIR/build
  fi
}

function build_libuvc {
  LIB_DIR=libuvc
  pushd .
  cd $LIB_DIR
  get_head
  git pull
  cd build
  cmake -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" ..
  make install
  # need to symlink to root directory
  ln -s x86_64-linux-gnu/libuvc.so.0 ${INSTALL_DIR}/lib/.
  popd
}

