#!/bin/bash

# idempotent function
function setup_jq {
  # jsoncpp
  if [ ! -d jq ] ; then
    git clone https://github.com/stedolan/jq.git
    git checkout jq-1.5
  fi 
}


function build_jq {
  pushd .
  cd jq
  get_head
  git pull
  ./autoreconf -i
  ./configure --prefix="${INSTALL_DIR}" --exec-prefix="${INSTALL_DIR}" --disable-maintainer-mode
  make install
  popd
}
