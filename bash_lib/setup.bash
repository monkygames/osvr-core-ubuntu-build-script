#!/bin/bash

# Handles setting up repositories and required packages for compilation.

# idempotent function
function setup {
  if [ ! -d $INSTALL_DIR ] ; then
    mkdir $INSTALL_DIR
  fi

  if [ ! -d $SETUP_DIR ] ; then
    mkdir $SETUP_DIR
  fi

  if [ $INSTALL_DEPS ] ; then
      if [ ! -f /etc/apt/sources.list.d/george-edison55-ubuntu-cmake-3_x-xenial.list ] ; then
        # check if ppas have been installed
        sudo apt-get --yes --force-yes install software-properties-common
      
        # Adding cmake ppa because requires cmake 3.x 
        # Ubuntu 14.04 only has cmake 2.x
        # Works on ubuntu 16.04 too
        sudo -E add-apt-repository -y ppa:george-edison55/cmake-3.x
        sudo -E add-apt-repository -y ppa:ubuntu-toolchain-r/test
        sudo -E apt-get update
      
        ## boost dependencies
        BOOST="libboost-dev libboost-thread-dev libboost-program-options-dev libboost-filesystem-dev"
      
        # The rest of the dependencies
        DEPS="cmake ${BOOST} git libopencv-dev python2.7 libusb-1.0-0-dev gcc-5 g++-5 git-core libeigen3-dev markdown libmarkdown2-dev libsdl2-dev libglew-dev"
        sudo apt-get --yes --force-yes install $DEPS
      fi

      # setup git (for pulls)
      git config --global user.email devops@monky-games.com
      git config --global user.name "DevOps"
  fi
}
