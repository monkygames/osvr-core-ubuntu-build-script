#!/bin/bash

function setup_vrpn {
  if [ ! -d vrpn ] ; then
    git clone https://github.com/vrpn/vrpn
  fi

  if [ ! -e vrpn/build ] ; then
    mkdir vrpn/build
  fi
}

function build_vrpn {
  pushd .
  cd vrpn
  if [ $LATEST_TAG == true ] ; then
      check_latest_tag
  else
      get_head
  fi
  cd build
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" -DCMAKE_MODULE_PATH:PATH="/usr/share/${CMAKE_DIR}/Modules" ..
  make
  #make install
  popd
}
