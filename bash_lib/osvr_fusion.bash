#!/bin/bash

# idempotent function
function setup_fusion {
  if [ ! -d OSVR-fusion ] ; then
    #git clone --recursive https://github.com/nanospork/OSVR-fusion
    git clone --recursive -b linux-compile https://github.com/Conzar/OSVR-fusion
  fi 

  if [ ! -d OSVR-fusion/build ] ; then
    mkdir OSVR-fusion/build
  fi
}


function build_fusion {
  pushd .
  cd OSVR-fusion
  # need to comment out in order to use the linux fix.
  #get_head
  git pull
  cd build
  cmake -DCMAKE_PREFIX_PATH="${INSTALL_DIR}" -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" ..
  make install
  popd
}
