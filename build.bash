#!/bin/bash

## === Argument Handling === ##
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

ROOT_DIR=$(pwd)
BASH_LIB_DIR=$(pwd)/bash_lib
build_steam=false
build_nolovr=false
build_fusion=false
use_help=false
no_clean=false
LATEST_TAG=false
TRACKER_FIX=true
INSTALL_DEPS=true

while getopts "d:snhrli:fp" opt; do
    case "$opt" in
    d)  
        ROOT_DIR=$OPTARG
        ;;  
    s)  build_steam=true
        ;; 
    n)  build_nolovr=true
        ;;
    f)  build_fusion=true
        ;;
    r)  no_clean=true
        ;; 
    l)  LATEST_TAG=true
        ;; 
    i)  BASH_LIB_DIR=$OPTARG
        ;; 
    h)  use_help=true
        ;;
    p)  INSTALL_DEPS=false
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

if [ $use_help == true ] ; then
  echo 'Usage: build [-d <path to root directory>] [-s]'
  echo '-d the root directory to install (optional)'
  echo '-s to build SteamVR-OSVR Plugin'
  echo '-n to build NolorVR-OSVR Plugin'
  echo '-f to build OSVR-Fusion Plugin'
  echo '-r to rebuild not using clean'
  echo '-l the latest tag (not master branch'
  echo '-i the directory that contains the bash libs'
  echo '-p set this flag to disable installation of dependencies'
  echo '-h to view help'
  exit 0;
fi

# Imports
source $BASH_LIB_DIR/common.bash
source $BASH_LIB_DIR/setup.bash
source $BASH_LIB_DIR/libfunctionality.bash
source $BASH_LIB_DIR/jsoncpp.bash
source $BASH_LIB_DIR/libuvc.bash
source $BASH_LIB_DIR/osvr_core.bash
source $BASH_LIB_DIR/render_manager.bash
source $BASH_LIB_DIR/open_scenegraph.bash
source $BASH_LIB_DIR/tracker_viewer.bash
source $BASH_LIB_DIR/steamvr.bash
source $BASH_LIB_DIR/video_status.bash
source $BASH_LIB_DIR/openvr.bash
source $BASH_LIB_DIR/nolovr.bash
source $BASH_LIB_DIR/hidapi.bash
source $BASH_LIB_DIR/osvr_fusion.bash
#source $BASH_LIB_DIR/.bash

## set the root directory for building
pushd .
cd $ROOT_DIR
## ====================== ##


## == Global Variables == ##
INSTALL_DIR=$(pwd)/dist
SETUP_DIR=$INSTALL_DIR/setup
LIBFUNCTIONALITY_DIR=$(pwd)/libfunctionality
JSONCPP_DIR=$(pwd)/jsoncpp
RENDER_MANAGER_DIR=$(pwd)/OSVR-RenderManager

## Make sure using g++-5
export CXX="g++-5" CC="gcc-5"
## ====================== ##

# ==== Setup Repos ==== #
setup
setup_libfunctionality
setup_jsoncpp
setup_libuvc
setup_osvr_core
setup_render_manager
setup_open_scenegraph
setup_tracker_viewer

if [ $build_steam == true ] ; then
  setup_steamvr
  #setup_openvr
fi

if [ $build_nolovr == true ] ; then
  setup_hidapi
  setup_nolovr
fi

if [ $build_fusion == true ] ; then
  setup_fusion
fi

# clean all
if [ $no_clean == false ] ; then
  clean libfunctionality
  clean jsoncpp
  clean OSVR-Core
  if [ $build_steam == true ] ; then
    clean SteamVR-OSVR
    clean openvr
  fi
  if [ $build_nolovr == true ] ; then
     clean hidapi
     clean nolo-osvr
  fi
fi

# build all
build_libfunctionality
build_jsoncpp
build_libuvc
build_osvr
build_rendermanager
build_open_scenegraph 
build_tracker_viewer
if [ $build_steam == true ] ; then
  echo '============ building steam'
  build_steamvr
  #build_openvr
fi
if [ $build_nolovr == true ] ; then
  echo '============ building nolovr'
  build_hidapi
  build_nolovr
fi
if [ $build_fusion == true ] ; then
  echo '============ building osvr-fusion'
  build_fusion
fi

# exit install dir
popd
